﻿namespace Tam.Lib.Model
{
  /// <summary>
  /// Represents a stored entity
  /// </summary>
  public interface IEntity
  {
    /// <summary>
    /// Entity identifier
    /// </summary>
    long Id { get; }
  }
}
