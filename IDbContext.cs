﻿namespace Tam.Lib.Model
{
  /// <summary>
  /// Database context
  /// </summary>
  public interface IDbContext
  {
    /// <summary>
    /// Save changes to database
    /// </summary>
    /// <returns>Number of records affected</returns>
    int SaveChanges();
  }
}
